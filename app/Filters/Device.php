<?php

namespace App\Filters;

use App\Models\Services\QueryService;
use App\Models\Services\VersionService;
use \Illuminate\Database\Eloquent\Builder;
use Kyslik\LaravelFilterable\GenericFilterable;

class Device extends GenericFilterable
{

    /**
     * Filter mapping - map method to filter parameters
     *
     * @return array
     */
    public function filterMap(): array
    {
        return [
            'serial' => ['serial'],
            'platform' => ['platform'],
            'customer' => ['customer'],
            'partner' => ['partner'],
            'version' => ['version'],
            'deviceStatus' => ['device_status'],
            'lastIp' => ['last_ip'],
            'lastPtr' => ['last_ptr'],
            'lastCheck' => ['last_check'],
            'createdAt' => ['created_at'],
            'softwareExpiration' => ['software_expiration'],
            'hardwareExpiration' => ['hardware_expiration'],
        ];
    }

    /**
     * Filter result by serial
     *
     * @param string|null $serial serial
     *
     * @return Builder
     */
    public function serial($serial = null): Builder
    {
        return $this->builder->where('serial', 'like', '%'.$serial.'%');
    }

    /**
     * Filter result by platform
     *
     * @param string|null $platform platform
     *
     * @return Builder
     */
    public function platform($platform = null): Builder
    {
        return $this->builder->select(['devices.*'])
            ->join('platforms as pl', 'devices.platform_id', 'pl.id')
            ->where('pl.name', 'like', '%'.$platform.'%');
    }

    /**
     * Filter result by customer
     *
     * @param string|null $customer customer
     *
     * @return Builder
     */
    public function customer($customer = null): Builder
    {
        return $this->builder->select(['devices.*'])
            ->join('customers as cs', 'devices.customer_id', 'cs.id')
            ->where('cs.name', 'like', '%'.$customer.'%');
    }

    /**
     * Filter result by partner
     *
     * @param string|null $partner partner
     *
     * @return Builder
     */
    public function partner($partner = null): Builder
    {
        return $this->builder->select(['devices.*'])
            ->join('customers as cs', 'devices.customer_id', 'cs.id')
            ->join('partners as pn', 'cs.partner_id', 'pn.id')
            ->where('pn.name', 'like', '%'.$partner.'%');
    }

    /**
     * Filter result by version
     *
     * @param string|null $version version
     *
     * @return Builder
     */
    public function version($version = null): Builder
    {
        return $this->builder->select(['devices.*'])
            ->join('device_history as dh', 'dh.device_id', 'devices.id')
            ->join('device_history_software_version as dhsv', 'dhsv.device_history_id', 'dh.id')
            ->join('software_versions as sv', 'sv.id', 'dhsv.software_version_id')
            ->where('sv.software_component_id', '=', 1)
            ->where('sv.version', 'like', '%'.VersionService::encode($version).'%')
            ->orderByDesc('dh.id')
            ->groupBy(
                [
                    'devices.id',
                    'devices.customer_id',
                    'devices.software_branch_id',
                    'devices.device_status_id',
                    'devices.platform_id',
                    'devices.uuid',
                    'devices.serial',
                    'devices.description',
                    'devices.hw_model',
                    'devices.created_at',
                    'devices.updated_at',
                ]
            );
    }

    /**
     * Filter result by device status
     *
     * @param string|null $deviceStatus device Status
     *
     * @return Builder
     */
    public function deviceStatus($deviceStatus = null): Builder
    {
        return $this->builder->select(['devices.*'])
            ->join('device_statuses as ds', 'devices.device_status_id', 'ds.id')
            ->where('ds.name', 'like', '%'.$deviceStatus.'%');
    }

    /**
     * Filter result by last ip
     *
     * @param string|null $lastIp last ip
     *
     * @return Builder
     */
    public function lastIp($lastIp = null): Builder
    {
        return $this->builder->select(['devices.*'])
            ->join('device_history as dh', 'dh.device_id', 'devices.id')
            ->where('dh.ip', 'like', '%'.$lastIp.'%')
            ->groupBy(
                [
                    'devices.id',
                    'devices.customer_id',
                    'devices.software_branch_id',
                    'devices.device_status_id',
                    'devices.platform_id',
                    'devices.uuid',
                    'devices.serial',
                    'devices.description',
                    'devices.hw_model',
                    'devices.created_at',
                    'devices.updated_at',
                ]
            );
    }

    /**
     * Filter result by last ptr
     *
     * @param string|null $lastPtr last ptr
     *
     * @return Builder
     */
    public function lastPtr($lastPtr = null): Builder
    {
        return $this->builder->select(['devices.*'])
            ->join('device_history as dh', 'dh.device_id', 'devices.id')
            ->where('dh.ptr', 'like', '%'.$lastPtr.'%')
            ->groupBy(
                [
                    'devices.id',
                    'devices.customer_id',
                    'devices.software_branch_id',
                    'devices.device_status_id',
                    'devices.platform_id',
                    'devices.uuid',
                    'devices.serial',
                    'devices.description',
                    'devices.hw_model',
                    'devices.created_at',
                    'devices.updated_at',
                ]
            );
    }

    /**
     * Filter result by last check
     *
     * @param string|null $lastCheck last check in format Y-m-d H:i:s
     *
     * @return Builder
     */
    public function lastCheck($lastCheck = null): Builder
    {
        $queryBuilder = $this->builder->select(['devices.*'])
            ->join('device_history as dh', 'dh.device_id', 'devices.id')
            ->groupBy(
                [
                    'devices.id',
                    'devices.customer_id',
                    'devices.software_branch_id',
                    'devices.device_status_id',
                    'devices.platform_id',
                    'devices.uuid',
                    'devices.serial',
                    'devices.description',
                    'devices.hw_model',
                    'devices.created_at',
                    'devices.updated_at',
                ]
            );
        return QueryService::dateTime($queryBuilder, 'dh.created_at', $lastCheck);
    }

    /**
     * Filter result by created at
     *
     * @param string|null $createdAt created at in format Y-m-d H:i:s
     *
     * @return Builder
     */
    public function createdAt($createdAt = null): Builder
    {
        return QueryService::dateTime($this->builder, 'devices.created_at', $createdAt);
    }

    /**
     * Filter result by software expiration date
     *
     * @param string|null $softwareExpiration software expiration date in format Y-m-d
     *
     * @return Builder
     */
    public function softwareExpiration($softwareExpiration = null): Builder
    {
        $queryBuilder = $this->builder->select(['devices.*'])
            ->join('support_expiration as se', 'se.device_id', 'devices.id')
            ->where(['support_type_id' => 1]);
        return QueryService::dateTime($queryBuilder, 'se.expiration_date', $softwareExpiration, false);
    }

    /**
     * Filter result by hardware expiration date
     *
     * @param string|null $hardwareExpiration hardware expiration date in format Y-m-d
     *
     * @return Builder
     */
    public function hardwareExpiration($hardwareExpiration = null): Builder
    {
        $queryBuilder = $this->builder->select(['devices.*'])
            ->join('support_expiration as se', 'se.device_id', 'devices.id')
            ->where(['support_type_id' => 2]);
        return QueryService::dateTime($queryBuilder, 'se.expiration_date', $hardwareExpiration, false);
    }
}