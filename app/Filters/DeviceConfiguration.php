<?php

namespace App\Filters;

use \Illuminate\Database\Eloquent\Builder;
use Kyslik\LaravelFilterable\GenericFilterable;

class DeviceConfiguration extends GenericFilterable
{

    /**
     * Filter mapping - map method to filter parameters
     *
     * @return array
     */
    public function filterMap(): array
    {
        return [
            'device' => ['device'],
            'mgmtIp' => ['mgmt_ip'],
            'mgmtLogin' => ['mgmt_login'],
            'ip' => ['ip'],
            'systemLogin' => ['system_login'],
            'hasMgmt' => ['has_mgmt'],
            'hasAccess' => ['has_access'],
        ];
    }

    /**
     * Filter result by device
     *
     * @param string|null $device device
     *
     * @return Builder
     */
    public function device($device = null): Builder
    {
        return $this->builder->select(['device_configurations.*'])
            ->join('devices as d', 'device_configurations.device_id', 'd.id')
            ->where('d.serial', 'like', '%'.$device.'%');
    }

    /**
     * Filter result by mgmt ip
     *
     * @param string|null $mgmtIp mgmt ip
     *
     * @return Builder
     */
    public function mgmtIp($mgmtIp = null): Builder
    {
        return $this->builder->where('mgmt_ip', 'like', '%'.$mgmtIp.'%');
    }

    /**
     * Filter result by mgmt login
     *
     * @param string|null $mgmtLogin mgmt login
     *
     * @return Builder
     */
    public function mgmtLogin($mgmtLogin = null): Builder
    {
        return $this->builder->where('mgmt_login', 'like', '%'.$mgmtLogin.'%');
    }

    /**
     * Filter result by ip
     *
     * @param string|null $ip ip
     *
     * @return Builder
     */
    public function ip($ip = null): Builder
    {
        return $this->builder->where('ip', 'like', '%'.$ip.'%');
    }

    /**
     * Filter result by system login
     *
     * @param string|null $systemLogin system login
     *
     * @return Builder
     */
    public function systemLogin($systemLogin = null): Builder
    {
        return $this->builder->where('system_login', 'like', '%'.$systemLogin.'%');
    }

    /**
     * Filter result by has mgmt param
     *
     * @param string|null $hasMgmt has mgmt param
     *
     * @return Builder
     */
    public function hasMgmt($hasMgmt = null): Builder
    {
        $param = (strtolower($hasMgmt) == 'true') ? 1 : 0;
        return $this->builder->where('has_mgmt', $param);
    }

    /**
     * Filter result by has access param
     *
     * @param string|null $hasAccess has access param
     *
     * @return Builder
     */
    public function hasAccess($hasAccess = null): Builder
    {
        $param = (strtolower($hasAccess) == 'true') ? 1 : 0;
        return $this->builder->where('has_access', $param);
    }
}