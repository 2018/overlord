<?php

namespace App\Models;

use Config;
use App\Models\Services\VersionService;
use Kyslik\LaravelFilterable\FilterableTrait;
use App\Models\Traits\DestroyEntity;
use App\Models\Traits\SaveEntityRelations;
use App\Filters\SoftwareVersion as SoftwareVersionFilter;
use App\Http\Resources\SoftwareVersionList;
use App\Http\Resources\SoftwareVersion as SoftwareVersionResource;

class SoftwareVersion extends BaseModel
{
    use DestroyEntity,
        FilterableTrait,
        SaveEntityRelations;

    /**
     * Boot function from laravel.
     *
     * @return mixed
     */
    protected static function boot()
    {
        parent::boot();

        static::saving(
            function ($model) {
                $fileName = self::getImagePath($model->file_name);
                $model->checksum = hash_file('sha256', $fileName);
            }
        );
    }

    /**
     * Table name
     *
     * @var string
     */
    protected $table = 'software_versions';

    /**
     * Validating rules
     *
     * @var array
     */
    protected $rules = [
        'software_branch_id' => 'required|integer|exists:software_branches,id',
        'software_component_id' => 'required|integer|exists:software_components,id',
        'version'  => 'required|min:5|max:45',
        'file_name'  => 'required|string|image_exists|min:5|max:120',
        'is_enabled' => 'required|boolean',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'software_branch_id',
        'software_component_id',
        'version',
        'file_name',
        'is_enabled',
    ];

    /**
     * Array of references
     *
     * @var array
     */
    protected $references = [
        'platforms',
    ];

    /**
     * Encode version attribute to db format
     *
     * @param string $version password
     *
     * @return void
     */
    public function setVersionAttribute($version)
    {
        $this->attributes['version'] = VersionService::encode($version);
    }

    /**
     * Decode version attribute to short format
     *
     * @param string $version password
     *
     * @return string
     */
    public function getVersionAttribute($version)
    {
        return VersionService::trim($version);
    }

    /**
     * Return version image file path
     *
     * @param string $fileName file name
     *
     * @return string
     */
    public static function getImagePath($fileName)
    {
        $imagesPath = Config::get('constants.VERSION_IMAGES_PATH');
        return $imagesPath . DIRECTORY_SEPARATOR . $fileName;
    }

    /**
     * Return version image file url
     *
     * @param string $fileName file name
     *
     * @return string
     */
    public static function getImageUrl($fileName)
    {
        return url('images/' . $fileName);
    }

    /**
     * Return response download image file
     *
     * @param string $name image name
     *
     * @return array
     */
    public function downloadImage($name)
    {
        $path = self::getImagePath($name);
        return response()->download($path, $name);
    }

    /**
     * Get all of the models from the database.
     *
     * @param SoftwareVersionFilter $filters filters
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function collectionItems(SoftwareVersionFilter $filters)
    {
        $items = self::with(
            [
                'softwareBranch',
                'softwareComponent',
                'platforms',
            ]
        )->filter($filters)->get();
        //dd($items);
        return SoftwareVersionList::collection($items);
    }

    /**
     * Return resource item data
     *
     * @param string $id id
     *
     * @return array
     */
    public function editItem($id)
    {
        $res = new SoftwareVersionResource($this->find($id));
        return $res->toArray(0);
    }

    /**
     * Return resource item data
     *
     * @return array
     */
    public function createItem()
    {
        $res = new SoftwareVersionResource(new self());
        return $res->toArray(0);
    }

    /**
     * Define BelongsToMany relation to Platform
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function platforms()
    {
        return $this->belongsToMany(Platform::class);
    }

    /**
     * Define BelongsTo relation
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function softwareBranch()
    {
        return $this->belongsTo(SoftwareBranch::class);
    }

    /**
     * Define BelongsTo relation
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function softwareComponent()
    {
        return $this->belongsTo(SoftwareComponent::class);
    }

}
