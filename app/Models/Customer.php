<?php

namespace App\Models;

use App\Models\Traits\ShowEntity;
use App\Models\Traits\SaveEntity;
use App\Models\Traits\DestroyEntity;
use Kyslik\LaravelFilterable\FilterableTrait;
use App\Http\Resources\Customer as CustomerResource;
use App\Http\Resources\CustomerList;
use App\Filters\Customer as CustomerFilter;

class Customer extends BaseModel
{
    use ShowEntity,
        SaveEntity,
        DestroyEntity,
        FilterableTrait;

    /**
     * Table name
     *
     * @var string
     */
    protected $table = 'customers';

    /**
     * Validating rules
     *
     * @var array
     */
    protected $rules = [
        'country_id' => 'required|integer|exists:countries,id',
        'partner_id' => 'required|integer|exists:partners,id',
        'name'  =>  'required|unique:customers|min:1|max:45',
        'description' => 'max:255',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'country_id',
        'partner_id',
        'name',
        'description',
    ];

    /**
     * Get all of the models from the database.
     *
     * @param CustomerFilter $filters filters
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function collectionItems(CustomerFilter $filters)
    {
        $items = self::filter($filters)->get();
        return CustomerList::collection($items);
    }

    /**
     * Return resource item data
     *
     * @param string $id id
     *
     * @return array
     */
    public function editItem($id)
    {
        $res = new CustomerResource($this->find($id));
        return $res->toArray(0);
    }

    /**
     * Return resource item data
     *
     * @return array
     */
    public function createItem()
    {
        $res = new CustomerResource(new self());
        return $res->toArray(0);
    }

    /**
     * Define BelongsTo relation
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    /**
     * Define BelongsTo relation
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function partner()
    {
        return $this->belongsTo(Partner::class);
    }
}
