<?php

namespace App\Models;

use Illuminate\Http\Request;
use App\Models\Traits\ShowEntity;
use App\Models\Traits\SaveEntity;
use App\Models\Traits\DestroyEntity;
use Kyslik\LaravelFilterable\FilterableTrait;
use App\Http\Resources\DeviceConfiguration as DeviceConfigurationResource;
use App\Http\Resources\DeviceConfigurationList;
use App\Filters\DeviceConfiguration as DeviceConfigurationFilter;

class DeviceConfiguration extends BaseModel
{
    use ShowEntity,
        SaveEntity,
        DestroyEntity,
        FilterableTrait;

    /**
     * Table name
     *
     * @var string
     */
    protected $table = 'device_configurations';

    /**
     * Validating rules
     *
     * @var array
     */
    protected $rules = [
        'device_id' => 'required|integer|exists:devices,id',
        'mgmt_ip' => 'max:45',
        'mgmt_login' => 'max:45',
        'mgmt_password' => 'max:45',
        'ip' => 'max:45',
        'system_login' => 'max:45',
        'system_password' => 'max:45',
        'bios_password' => 'max:45',
        'has_mgmt' => 'required|boolean',
        'has_access' => 'required|boolean',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'device_id',
        'mgmt_ip',
        'mgmt_login',
        'mgmt_password',
        'ip',
        'system_login',
        'system_password',
        'bios_password',
        'has_mgmt',
        'has_access',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'mgmt_password',
        'system_password',
        'bios_password',
    ];

    /**
     * Get all of the models from the database.
     *
     * @param DeviceConfigurationFilter $filters filters
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function collectionItems(DeviceConfigurationFilter $filters)
    {
        $items = self::with(
            [
                'device',
            ]
        )->filter($filters)->get();
        return DeviceConfigurationList::collection($items);
    }

    /**
     * Define BelongsTo relation
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function device()
    {
        return $this->belongsTo(Device::class);
    }

    /**
     * Return resource item data
     *
     * @param Request $request request
     * @param string  $id      id
     *
     * @return array
     */
    public function editItem(Request $request, $id)
    {
        $res = new DeviceConfigurationResource($this->find($id));
        return $res->toArray($request);
    }

    /**
     * Return resource item data
     *
     * @return array
     */
    public function createItem()
    {
        $res = new DeviceConfigurationResource(new self());
        return $res->toArray(0);
    }
}
