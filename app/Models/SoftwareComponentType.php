<?php

namespace App\Models;

use App\Models\Traits\ShowEntity;
use App\Models\Traits\SaveEntity;
use App\Models\Traits\DestroyEntity;

class SoftwareComponentType extends BaseModel
{
    use ShowEntity,
        SaveEntity,
        DestroyEntity;

    /**
     * Table name
     *
     * @var string
     */
    protected $table = 'software_component_types';

    /**
     * Validating rules
     *
     * @var array
     */
    protected $rules = [
        'name'  =>  'required|unique:software_component_types|min:1|max:45',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
    ];
}
