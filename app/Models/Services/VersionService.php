<?php

namespace App\Models\Services;

class VersionService
{
    /**
     * Trim left zero from version
     *
     * @param string $version version
     *
     * @return string
     */
    public static function trim($version)
    {
        $res = [];
        $parts = explode('.', $version);
        foreach($parts as $part) {
            $trimmedPart = trim($part);
            $res[] = ($trimmedPart == 0) ? 0 : ltrim(trim($part), '0');
        }
        return implode('.', $res);
    }

    /**
     * Encode version to system format
     *
     * @param string $version version
     *
     * @return string
     */
    public static function encode($version)
    {
        $res = [];
        $version = self::trim($version);
        $parts = explode('.', $version);
        foreach($parts as $part) {
            $trimmedPart = trim($part);
            $count = count(str_split($trimmedPart));
            $repeat = 6 - $count;
            $sPart = str_repeat('0', $repeat) . $trimmedPart;
            $res[] = $sPart;
        }
        return implode('.', $res);
    }
}