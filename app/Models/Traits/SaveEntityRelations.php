<?php

namespace App\Models\Traits;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

trait SaveEntityRelations
{
    use SaveRelations,
        ValidateBeforeSave;

    /**
     * Saves entity with relations
     *
     * @param Request     $request input params
     * @param null|string $id      entity id
     *
     * @return object
     *
     * @throws \Exception
     */
    public function saveEntity(Request $request, $id = null)
    {
        $params = $request->all();
        $model = $this;
        if (!is_null($id)) {
            $model = $this->findOrFail($id);
        }
        $model->fill($params);
        $model->fillRelations($params);
        DB::transaction(
            function () use ($model) {
                self::validateBeforeSave($model);
                $model->save();
                $model->saveRelations();
            }
        );
        return redirect(route($this->getTable(). '.edit', ['id' => $model->id]))->with('status', _i('Record has been saved'));
    }
}