<?php

namespace App\Models\Traits;

use Illuminate\Support\Facades\DB;

trait DestroyEntity
{
    use DestroyValidate;

    /**
     * Destroy entity
     *
     * @param string $id entity id
     *
     * @return array model data
     */
    public function destroyEntity($id)
    {
        $model = $this->findOrFail($id);
        DB::transaction(
            function () use ($model) {
                self::destroyValidate($model);
                $model->delete();
            }
        );
        return redirect(route($this->getTable(). '.index'))->with('status', _i('Record has been deleted'));
    }
}