<?php

namespace App\Models\Traits;

use Illuminate\Routing\Route;
use App\Exceptions\NotFoundException;

trait ListRelations
{
    /**
     * Returns transformed list
     *
     * @param string $id    entity id
     * @param Route  $route current route
     *
     * @return array
     */
    public function listRelations($id, Route $route)
    {
        $model = $this->findOrFail($id);
        $aRelatedEntity = explode(".", $route->getName());
        $relatedEntity = (array_has($aRelatedEntity, 1) and strlen($aRelatedEntity[1]) > 0) ? camel_case($aRelatedEntity[1]) : null;
        return $model->$relatedEntity->toArray();
    }
}