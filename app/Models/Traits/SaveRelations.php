<?php

namespace App\Models\Traits;

trait SaveRelations
{
    /**
     * Save all relations
     *
     * @return bool
     */
    protected function saveRelations()
    {
        $result = false;
        foreach ($this->relations as $relation => $ids) {
            $this->$relation()->sync($ids);
            $result = true;
        }
        return $result;
    }

    /**
     * Fill model relations
     *
     * @param array $param input params
     *
     * @return self
     */
    protected function fillRelations(array $param)
    {
        $relations = array_where(
            $param,
            function ($value, $key) {
                unset($value);
                return (in_array($key, $this->references) and method_exists($this->getModel(), $key));
            }
        );
        $relations = array_map(
            function ($value) {
                return array_filter($value);
            },
            $relations
        );
        return $this->setRelations($relations);
    }
}