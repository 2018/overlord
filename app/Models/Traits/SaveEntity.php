<?php

namespace App\Models\Traits;

use Validator;
use Illuminate\Http\Request;

trait SaveEntity
{
    /**
     * Saves entity
     *
     * @param Request     $request input params
     * @param null|string $id      entity id
     *
     * @return array model data
     *
     * @throws \Exception
     */
    public function saveEntity(Request $request, $id = null)
    {
        $params = $request->all();
        $model = $this;
        if (!is_null($id)) {
            $model = $this->findOrFail($id);
        }
        $model->fill($params);
        $model->saveOrFail();
        return redirect(route($this->getTable(). '.edit', ['id' => $model->id]))->with('status', _i('Record has been saved'));
    }
}