<?php

namespace App\Models\Traits;

use App\Models\BaseModel;
use App\Models\Services\ValidationService;

trait ValidateBeforeSave
{
    /**
     * Model validation with additional xml relations validating
     *
     * @param BaseModel $model entity object
     *
     * @return void
     *
     * @throws \Exception
     */
    public static function validateBeforeSave(BaseModel $model)
    {
        $validator = ValidationService::prepareValidator($model);
        $validator->validate();
    }
}