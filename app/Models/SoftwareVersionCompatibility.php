<?php

namespace App\Models;

use DB;
use Config;
use Illuminate\Http\Request;
use App\Http\Resources\SoftwareVersionCompatibilityList;
use App\Http\Resources\SoftwareVersionCompatibility as SoftwareVersionCompatibilityResource;

class SoftwareVersionCompatibility extends BaseModel
{
    /**
     * Table name
     *
     * @var string
     */
    protected $table = 'software_version_compatibility';

    /**
     * Validating rules
     *
     * @var array
     */
    protected $rules = [
        'software_version_id' => 'required|integer|exists:software_versions,id',
        'min_version_id' => 'nullable|integer|exists:software_versions,id|version_component',
        'max_version_id' => 'nullable|integer|exists:software_versions,id|required_without:min_version_id|version_component',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'software_version_id',
        'min_version_id',
        'max_version_id',
    ];

    /**
     * Get all of the models from the database.
     *
     * @param string $softwareVersionId software version id
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function collectionItems($softwareVersionId)
    {
        $items = self::with(['softwareMinVersion', 'softwareMaxVersion'])
            ->where('software_version_id', $softwareVersionId)
            ->get();
        return SoftwareVersionCompatibilityList::collection($items);
    }

    /**
     * Return resource item data
     *
     * @param Request $request software version id
     *
     * @return array
     */
    public function create(Request $request)
    {
        $res = new SoftwareVersionCompatibilityResource(new self());
        return $res->toArray($request);
    }

    /**
     * Saves entity
     *
     * @param Request $request input params
     *
     * @return array model data
     *
     * @throws \Exception
     * @throws \Throwable
     */
    public function storeEntity(Request $request)
    {
        $params = $request->all();
        $softwareVersionId = $request->route()->parameter('software_version');
        $params += ['software_version_id' => $softwareVersionId];
        $model = $this;
        $model->fill($params);
        $model->saveOrFail();
        return redirect(route('software_versions.edit', ['id' => $softwareVersionId]))->with('status', _i('Record has been saved'));
    }

    /**
     * Destroy entity
     *
     * @param Request $request input params
     *
     * @return array model data
     */
    public function destroyEntity(Request $request)
    {
        $model = $this->findOrFail($request->route()->parameter('compatibility'));
        DB::transaction(
            function () use ($model) {
                $model->delete();
            }
        );
        $redirect = route(
            'software_versions.edit',
            ['id' => $request->route()->parameter('software_version')]
        );
        return redirect($redirect)->with('status', _i('Record has been deleted'));
    }

    /**
     * Define BelongsTo relation
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function softwareVersion()
    {
        return $this->belongsTo(SoftwareVersion::class);
    }

    /**
     * Define BelongsTo relation
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function softwareMinVersion()
    {
        return $this->belongsTo(SoftwareVersion::class, 'min_version_id');
    }

    /**
     * Define BelongsTo relation
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function softwareMaxVersion()
    {
        return $this->belongsTo(SoftwareVersion::class, 'max_version_id');
    }
}
