<?php

namespace App\Models;

use App\Models\Traits\SaveEntity;
use App\Models\Traits\DestroyEntity;

class SoftwareBranch extends BaseModel
{
    use SaveEntity,
        DestroyEntity;

    /**
     * Table name
     *
     * @var string
     */
    protected $table = 'software_branches';

    /**
     * Validating rules
     *
     * @var array
     */
    protected $rules = [
        'type'  =>  'required|unique:software_branches|min:1|max:45',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'type',
    ];
}
