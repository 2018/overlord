<?php

namespace App\Models;

use App\Models\Traits\ShowEntity;
use App\Models\Traits\SaveEntity;
use App\Models\Traits\DestroyEntity;
use Kyslik\LaravelFilterable\FilterableTrait;
use App\Http\Resources\SupportExpirationList;
use App\Http\Resources\SupportExpiration as SupportExpirationResource;
use App\Filters\SupportExpiration as SupportExpirationFilter;

class SupportExpiration extends BaseModel
{
    use ShowEntity,
        SaveEntity,
        DestroyEntity,
        FilterableTrait;

    /**
     * Table name
     *
     * @var string
     */
    protected $table = 'support_expiration';

    /**
     * Validating rules
     *
     * @var array
     */
    protected $rules = [
        'device_id' => 'required|integer|exists:devices,id',
        'support_type_id' => 'required|integer|exists:support_types,id',
        'description' => 'max:255',
        'expiration_date' => 'required|date_format:Y-m-d',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'device_id',
        'support_type_id',
        'description',
        'expiration_date',
    ];

    /**
     * Add support expiration for software
     *
     * @param Device $device device model
     *
     * @return void
     *
     * @throws \Throwable
     */
    public static function addSoftware(Device $device)
    {
        $model = new self();
        $date = date('Y-m-d', strtotime('+ 1 year'));
        $supportType = SupportType::where('name', 'software')->first();
        $data = [
            'device_id' => $device->getAttribute('id'),
            'support_type_id' => $supportType->getAttribute('id'),
            'expiration_date' => $date,
        ];
        $model->fill($data);
        $model->saveOrFail();
    }

    /**
     * Add support expiration for hardware
     *
     * @param Device $device device model
     *
     * @return void
     *
     * @throws \Throwable
     */
    public static function addHardware(Device $device)
    {
        $model = new self();
        $date = date('Y-m-d', strtotime('+ 3 year'));
        $supportType = SupportType::where('name', 'hardware')->first();
        $data = [
            'device_id' => $device->getAttribute('id'),
            'support_type_id' => $supportType->getAttribute('id'),
            'expiration_date' => $date,
        ];
        $model->fill($data);
        $model->saveOrFail();
    }

    /**
     * Get all of the models from the database.
     *
     * @param SupportExpirationFilter $filters filters
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function collectionItems(SupportExpirationFilter $filters)
    {
        $items = self::with(
            [
                'device',
                'supportType',
            ]
        )->filter($filters)->get();
        return SupportExpirationList::collection($items);
    }

    /**
     * Return resource item data
     *
     * @param string $id id
     *
     * @return array
     */
    public function editItem($id)
    {
        $res = new SupportExpirationResource($this->find($id));
        return $res->toArray(0);
    }

    /**
     * Define BelongsTo relation
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function device()
    {
        return $this->belongsTo(Device::class);
    }

    /**
     * Define BelongsTo relation
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function supportType()
    {
        return $this->belongsTo(SupportType::class);
    }
}
