<?php

namespace App\Models;

use App\Models\Traits\SaveEntity;
use App\Models\Traits\DestroyEntity;

class Platform extends BaseModel
{
    use SaveEntity,
        DestroyEntity;

    /**
     * Table name
     *
     * @var string
     */
    protected $table = 'platforms';

    /**
     * Validating rules
     *
     * @var array
     */
    protected $rules = [
        'name'  =>  'required|unique:platforms|min:1|max:45',
        'description' => 'max:255',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'description',
    ];
}
