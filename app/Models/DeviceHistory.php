<?php

namespace App\Models;

use DB;
use Request;
use App\Http\Resources\DeviceHistoryList;
use Kyslik\LaravelFilterable\FilterableTrait;
use App\Filters\DeviceHistory as DeviceHistoryFilter;

class DeviceHistory extends BaseModel
{
    use FilterableTrait;

    /**
     * Disable updated_at col
     */
    const UPDATED_AT = null;

    /**
     * Enable timestamps
     *
     * @var array
     */
    public $timestamps = true;

    /**
     * Table name
     *
     * @var string
     */
    protected $table = 'device_history';

    /**
     * Validating rules
     *
     * @var array
     */
    protected $rules = [
        'device_id' => 'required|integer|exists:devices,id',
        'ip' => 'max:45',
        'ptr' => 'max:45',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'device_id',
        'ip',
        'ptr',
    ];

    /**
     * Hidden columns from output
     *
     * @var array
     */
    protected $hidden = [
        'created_at',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
    ];

    /**
     * Add device history row
     *
     * @param int $deviceId  device id
     * @param int $versionId version id
     *
     * @return DeviceHistory
     *
     * @throws \Throwable
     */
    public static function addItem($deviceId, $versionId)
    {
        $model = new self();
        $data = [
            'device_id' => $deviceId,
            'ip' => Request::ip(),
            'ptr' => gethostbyaddr(Request::ip()),
        ];
        $model->fill($data);
        DB::transaction(
            function () use ($model, $versionId) {
                $model->save();
                $model->softwareVersions()->sync([$versionId]);
            }
        );
        return $model;
    }

    /**
     * Define BelongsToMany relation
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function softwareVersions()
    {
        return $this->belongsToMany(SoftwareVersion::class);
    }

    /**
     * Get all of the models from the database.
     *
     * @param integer             $deviceId device id
     * @param DeviceHistoryFilter $filters  filters
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function collectionItems($deviceId, DeviceHistoryFilter $filters)
    {
        $items = self::with(
            [
                'softwareVersions',
            ]
        )->filter($filters)->where('device_id', $deviceId)->orderByDesc('id')->limit(100)->get();
        return DeviceHistoryList::collection($items);
    }
}
