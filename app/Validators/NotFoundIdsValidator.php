<?php

namespace App\Validators;

use DB;

class NotFoundIdsValidator implements ValidatorInterface
{

    /**
     * Checks whether the value contains not exists entity ids
     *
     * @param string $value      input value
     * @param array  $attribute  array of attributes
     * @param array  $parameters array of params
     * @param object $validator  instance of Illuminate\Support\Facades\Validator
     *
     * @return bool
     */
    public static function validate($value, $attribute, $parameters, $validator)
    {
        $result = true;
        unset($validator);
        $tableName = array_get($parameters, '0');
        $value = array_filter($value);
        $countIds = DB::table($tableName)->whereIn(array_get($parameters, '1', 'id'), $value)->count();
        if (count($value) != $countIds) {
            $result = false;
        }
        return $result;
    }
}