<?php

namespace App\Validators;

class IsReferencedValidator implements ValidatorInterface
{

    /**
     * Checks whether is_referenced property equals true
     *
     * @param string $value      input value
     * @param array  $attribute  array of attributes
     * @param array  $parameters array of params
     * @param object $validator  instance of Illuminate\Support\Facades\Validator
     *
     * @return bool
     */
    public static function validate($value, $attribute, $parameters, $validator)
    {
        $result = true;
        unset($value, $attribute, $parameters);
        $data = $validator->getData();
        if (array_has($data, 'is_referenced') and $data['is_referenced'] === true) {
            $result = false;
        }
        return $result;
    }
}