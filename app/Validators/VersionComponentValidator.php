<?php

namespace App\Validators;

use DB;
use Validator;

class VersionComponentValidator implements ValidatorInterface
{

    /**
     * Checks whether the software version component is logmanager
     *
     * @param string $value      input value
     * @param array  $attribute  array of attributes
     * @param array  $parameters array of params
     * @param object $validator  instance of Illuminate\Support\Facades\Validator
     *
     * @return bool
     */
    public static function validate($value, $attribute, $parameters, $validator)
    {
        $result = true;
        unset($attribute, $parameters, $validator);
        $softwareVersion = DB::table('software_versions')
            ->select('software_component_id')
            ->where('id', $value)
            ->first();
        if ($softwareVersion and data_get($softwareVersion, 'software_component_id') !== 1) {
            $result = false;
        }
        return $result;
    }
}