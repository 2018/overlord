<?php

namespace App\Http\Controllers;

use Validator;
use App\Models\Partner;
use App\Filters\Partner as PartnerFilter;
use App\Http\Controllers\Traits\Show;
use App\Http\Controllers\Traits\Edit;
use App\Http\Controllers\Traits\Store;
use App\Http\Controllers\Traits\Create;
use App\Http\Controllers\Traits\Update;
use App\Http\Controllers\Traits\Destroy;

class PartnerController extends AdminController
{
    use Show,
        Edit,
        Store,
        Create,
        Update,
        Destroy;

    protected $model;

    /**
     * PartnerController constructor.
     *
     * @param Partner $model Partner model
     */
    public function __construct(Partner $model)
    {
        parent::__construct();
        $this->model = $model;
    }

    /**
     * Display a listing of the resource.
     *
     * @param PartnerFilter $filters filters
     *
     * @return \Illuminate\Http\Response
     */
    public function index(PartnerFilter $filters)
    {
        return view($this->model->getTable() . '.index', ['data' => $this->model->collectionItems($filters)]);
    }
}
