<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use App\Models\SoftwareVersionCompatibility;

class SoftwareVersionCompatibilityController extends AdminController
{
    protected $model;

    /**
     * SoftwareVersionCompatibilityController constructor.
     *
     * @param SoftwareVersionCompatibility $model SoftwareVersionCompatibility model
     */
    public function __construct(SoftwareVersionCompatibility $model)
    {
        parent::__construct();
        $this->model = $model;
    }

    /**
     * Display create page
     *
     * @param Request $request request
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        return view('software_version_compatibility.create', ['data' => $this->model->create($request)]);
    }

    /**
     * Store resource
     *
     * @param Request $request input params
     *
     * @return mixed
     *
     * @throws \Exception
     * @throws \Throwable
     */
    public function store(Request $request)
    {
        return $this->model->storeEntity($request);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request input params
     *
     * @return array
     */
    public function destroy(Request $request)
    {
        return $this->model->destroyEntity($request);
    }
}
