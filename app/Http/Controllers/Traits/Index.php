<?php

namespace App\Http\Controllers\Traits;

trait Index
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view($this->model->getTable() . '.index', ['data' => $this->model->collectionItems()]);
    }
}
