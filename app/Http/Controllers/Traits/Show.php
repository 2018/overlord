<?php

namespace App\Http\Controllers\Traits;

trait Show
{
    /**
     * Display the specified resource.
     *
     * @param int $id param id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view($this->model->getTable() . '.show', ['model' => $this->model->find($id)]);
    }
}
