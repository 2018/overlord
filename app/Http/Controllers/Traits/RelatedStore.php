<?php

namespace App\Http\Controllers\Traits;

use Illuminate\Http\Request;
use Illuminate\Routing\Route;

trait RelatedStore
{
    /**
     * Store resource relations
     *
     * @param Request $request input params
     * @param string  $id      entity id
     * @param Route   $route   route
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id, Route $route)
    {
        return $this->respond($this->model->saveRelations($request, $id, $route), 201);
    }

}
