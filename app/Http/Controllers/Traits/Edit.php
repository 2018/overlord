<?php

namespace App\Http\Controllers\Traits;

trait Edit
{

    /**
     * Display edit page
     *
     * @param integer $id entity id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view($this->model->getTable() . '.edit', ['data' => $this->model->editItem($id)]);
    }
}
