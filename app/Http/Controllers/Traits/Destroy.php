<?php

namespace App\Http\Controllers\Traits;

trait Destroy
{
    /**
     * Remove the specified resource from storage.
     *
     * @param int $id param id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return $this->model->destroyEntity($id);
    }

}
