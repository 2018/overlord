<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class SoftwareVersionCompatibilityList extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request Request
     *
     * @return array
     */
    public function toArray($request)
    {
        unset($request);
        return [
            'min_version' => $this->getAttribute('softwareMinVersion') ? $this->getAttribute('softwareMinVersion')->getAttribute('version') : '',
            'max_version' => $this->getAttribute('softwareMaxVersion') ? $this->getAttribute('softwareMaxVersion')->getAttribute('version') : '',
            'delete_button' =>  ['software_version.compatibility.destroy', $this->getAttribute('software_version_id'), $this->getAttribute('id')],
        ];
    }
}
