<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class SoftwareVersionList extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request Request
     *
     * @return array
     */
    public function toArray($request)
    {
        unset($request);
        return [
            'version' => $this->getAttribute('version'),
            'file_name' => $this->getAttribute('file_name'),
            'software_component' => $this->getAttribute('softwareComponent')->getAttribute('name'),
            'software_branch' => $this->getAttribute('softwareBranch')->getAttribute('type'),
            'platforms' => $this->getAttribute('platforms')->pluck('name'),
            'is_enabled' => ($this->getAttribute('is_enabled') ? _i('True') : _i('False')),
            'edit_button' =>  route($this->getTable() . '.edit', $this->getAttribute('id')),
            'delete_button' =>  [$this->getTable() . '.destroy', $this->getAttribute('id')],
        ];
    }
}
