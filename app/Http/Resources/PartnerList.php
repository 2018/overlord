<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class PartnerList extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request Request
     *
     * @return array
     */
    public function toArray($request)
    {
        unset($request);
        return [
            'country' => $this->getAttribute('country')->getAttribute('name'),
            'name' => $this->getAttribute('name'),
            'type' => $this->getAttribute('type'),
            'edit_button' =>  route($this->getTable() . '.edit', $this->getAttribute('id')),
            'delete_button' =>  [$this->getTable() . '.destroy', $this->getAttribute('id')],
        ];
    }
}
