<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class DeviceHistoryList extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request Request
     *
     * @return array
     */
    public function toArray($request)
    {
        unset($request);
        $softwareVersionRel = $this->getAttribute('softwareVersions');
        $softwareVersion = !is_null($softwareVersionRel) ? $softwareVersionRel->last() : null;
        $softwareComponent = !is_null($softwareVersion) ? $softwareVersion->getAttribute('softwareComponent') : null;
        $softwareBranch = !is_null($softwareVersion) ? $softwareVersion->getAttribute('softwareBranch') : null;
        return [
            'version' => (!is_null($softwareVersion) ? $softwareVersion->getAttribute('version') : ''),
            'last_ip' => $this->getAttribute('ip'),
            'last_ptr' => $this->getAttribute('ptr'),
            'last_check' => $this->getAttribute('created_at'),
            'software_component' => (!is_null($softwareComponent) ? $softwareComponent->getAttribute('name') : ''),
            'software_branch' => (!is_null($softwareBranch) ? $softwareBranch->getAttribute('type') : ''),
        ];
    }
}
