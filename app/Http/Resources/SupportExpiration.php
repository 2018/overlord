<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class SupportExpiration extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request Request
     *
     * @return array
     */
    public function toArray($request)
    {
        unset($request);
        return [
            'id' => $this->getAttribute('id'),
            'device_id' => $this->getAttribute('device_id'),
            'support_type_id' => $this->getAttribute('support_type_id'),
            'resource' => $this->resource,
            'description' => $this->getAttribute('description'),
            'expiration_date' => $this->getAttribute('expiration_date'),
            'support_types' => \App\Models\SupportType::pluck('name', 'id')->toArray(),
            'devices' => \App\Models\Device::pluck('serial', 'id')->toArray(),
        ];
    }
}
