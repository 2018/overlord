<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class SoftwareVersionCompatibility extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request request
     *
     * @return array
     */
    public function toArray($request)
    {
        $softwareVersionId = $request->route()->parameter('software_version');
        $softwareVersions = self::getSoftwareVersions();
        return [
            'id' => $this->getAttribute('id'),
            'software_version_name' => array_get($softwareVersions, $softwareVersionId),
            'software_version_id' => $request->route()->parameter('software_version'),
            'min_version_id' => $this->getAttribute('min_version_id'),
            'max_version_id' => $this->getAttribute('max_version_id'),
            'resource' => $this->resource,
            'software_versions' => $softwareVersions,
        ];
    }

    /**
     * Return assoc array of Software Versions sorted by id
     *
     * @return mixed
     */
    protected static function getSoftwareVersions()
    {
        $versions = \App\Models\SoftwareVersion::where('software_component_id', 1)->pluck('version', 'id')->toArray();
        ksort($versions);
        return $versions;
    }
}
