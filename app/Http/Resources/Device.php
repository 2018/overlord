<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class Device extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request Request
     *
     * @return array
     */
    public function toArray($request)
    {
        unset($request);
        return [
            'id' => $this->getAttribute('id'),
            'customer_id' => $this->getAttribute('customer_id'),
            'customers' => self::getCustomers(),
            'resource' => $this->resource,
            'description' => $this->getAttribute('description'),
        ];
    }

    /**
     * Return assoc array of customers sorted by id
     *
     * @return mixed
     */
    protected static function getCustomers()
    {
        $country = \App\Models\Customer::pluck('name', 'id')->toArray();
        ksort($country);
        return $country;
    }
}
