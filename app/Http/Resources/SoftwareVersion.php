<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;
use App\Models\SoftwareVersionCompatibility;

class SoftwareVersion extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request Request
     *
     * @return array
     */
    public function toArray($request)
    {
        unset($request);
        $items = SoftwareVersionCompatibility::with(['softwareMinVersion', 'softwareMaxVersion'])
            ->where('software_version_id', $this->getAttribute('id'))->get();
        $compatibility = SoftwareVersionCompatibilityList::collection($items);

        return [
            'id' => $this->getAttribute('id'),
            'software_branch_id' => $this->getAttribute('software_branch_id'),
            'software_component_id' => $this->getAttribute('software_component_id'),
            'resource' => $this->resource,
            'version' => $this->getAttribute('version'),
            'file_name' => $this->getAttribute('file_name'),
            'software_components' => self::getComponents(),
            'software_branches' => self::getBranches(),
            'platforms' => self::getPlatforms(),
            'platform_ids' => $this->getAttribute('platforms')->pluck('id')->toArray(),
            'is_enabled' => ($this->getAttribute('is_enabled') ? _i('True') : _i('False')),
            'compatibility' => $compatibility,
        ];
    }

    /**
     * Return assoc array of platforms sorted by id
     *
     * @return mixed
     */
    protected static function getPlatforms()
    {
        $platforms = \App\Models\Platform::pluck('name', 'id')->toArray();
        ksort($platforms);
        return $platforms;
    }

    /**
     * Return assoc array of software components sorted by id
     *
     * @return mixed
     */
    protected static function getComponents()
    {
        $components = \App\Models\SoftwareComponent::pluck('name', 'id')->toArray();
        ksort($components);
        return $components;
    }

    /**
     * Return assoc array of software branches sorted by id
     *
     * @return mixed
     */
    protected static function getBranches()
    {
        $branches = \App\Models\SoftwareBranch::pluck('type', 'id')->toArray();
        ksort($branches);
        return $branches;
    }
}
