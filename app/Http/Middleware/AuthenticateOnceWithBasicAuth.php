<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class AuthenticateOnceWithBasicAuth
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request request
     * @param \Closure                 $next    closure
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        return Auth::onceBasic('name') ?: $next($request);
    }
}
