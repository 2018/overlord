@extends('layouts.app')
@section('title', _i('Devices list'))
@section('content')
    {{ Html::bsList($data,
    [
        'serial' => [_i('Serial number'), true],
        'platform' => [_i('Platform'), true],
        'customer' => [_i('Customer'), true],
        'partner' => [_i('Partner'), true],
        'version' => [_i('Version'), true],
        'device_status' => [_i('Status'), true],
        'last_ip' => [_i('Last IP'), true],
        'last_ptr' => [_i('Last PTR'), true],
        'last_check' => [_i('Last check'), true],
        'created_at' => [_i('Created at'), true],
        'software_expiration' => [_i('Software expiration'), true],
        'hardware_expiration' => [_i('Hardware expiration'), true],
    ]) }}
@endsection
