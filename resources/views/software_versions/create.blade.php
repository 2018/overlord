@extends('layouts.app')
@section('title', _i('Create software version'))
@section('content')
    {{ Form::open(['route' => 'software_versions.store']) }}
    @include('software_versions.form_fields')
    {{ Form::close() }}
@endsection
