@extends('layouts.app')
@section('title', _i('Update software version'))
@section('content')
    {{ Form::model(array_get($data, 'resource'), ['route' => ['software_versions.update', array_get($data, 'id')], 'method' => 'put']) }}
    @include('software_versions.form_fields')
    {{ Form::close() }}
    @include('software_version_compatibility.index')
@endsection
