@extends('layouts.app')
@section('title', _i('Device configuration list'))
@section('list_buttons')
    <a href="{{ route('device_configurations.create') }}">{{ _i('Create') }}</a>
@endsection
@section('content')
    {{ Html::bsList($data,
    [
        'device' => [_i('Device serial'), true],
        'mgmt_ip' => [_i('MGMT ip'), true],
        'mgmt_login' => [_i('MGMT login'), true],
        'ip' => [_i('IP'), true],
        'system_login' => [_i('System login'), true],
        'has_mgmt' => [_i('Has MGMT'), true],
        'has_access' => [_i('Has access'), true],
    ]) }}
@endsection
