@extends('layouts.app')
@section('title', _i('Create device configuration'))
@section('content')
    {{ Form::open(['route' => 'device_configurations.store']) }}
    @include('device_configurations.form_fields')
    {{ Form::close() }}
@endsection
