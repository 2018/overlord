<br>
<div class="row">
    <div class="col-md-2"></div>
    <div class="col-md-8">
        <h3 class="pull-left">{{ _i("Version compatibility") }}</h3> <p class="text-right"><a href="{{ route('software_version.compatibility.create', array_get($data, 'id')) }}">{{ _i('Create') }}</a></p>
    </div>
</div>
<div class="row">
    <div class="col-md-2"></div>
    <div class="col-md-8">
        {{ Html::bsList(data_get($data, 'compatibility'), [
            'min_version' => [_i('Minimum version')],
            'max_version' => [_i('Maximum version')],
        ]) }}
    </div>
</div>
