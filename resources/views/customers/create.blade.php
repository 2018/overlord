@extends('layouts.app')
@section('title', _i('Create customer'))
@section('content')
    {{ Form::open(['route' => 'customers.store']) }}
    @include('customers.form_fields')
    {{ Form::close() }}
@endsection
