@extends('layouts.app')
@section('title', _i('Customers list'))
@section('list_buttons')
    <a href="{{ route('customers.create') }}">{{ _i('Create') }}</a>
@endsection
@section('content')
    {{ Html::bsList($data,
    [
        'name' => [_i('Customer name'), true],
        'country' => [_i('Country'), true],
        'partner' => [_i('Partner'), true],
        'description' => [_i('Description'), true],
    ]) }}
@endsection
