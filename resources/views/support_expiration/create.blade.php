@extends('layouts.app')
@section('title', _i('Create support expiration'))
@section('content')
    {{ Form::open(['route' => 'support_expiration.store']) }}
    @include('support_expiration.form_fields')
    {{ Form::close() }}
@endsection
