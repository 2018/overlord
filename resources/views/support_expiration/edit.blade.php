@extends('layouts.app')
@section('title', _i('Update support expiration'))
@section('content')
    {{ Form::model(array_get($data, 'resource'), ['route' => ['support_expiration.update', array_get($data, 'id')], 'method' => 'put']) }}
    @include('support_expiration.form_fields')
    {{ Form::close() }}
@endsection
