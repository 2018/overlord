@extends('layouts.app')
@section('title', _i('Users list'))
@section('list_buttons')
    <a href="{{ route('users.create') }}">{{ _i('Create') }}</a>
@endsection
@section('content')
    {{ Html::bsList($data, [
        'name' => [_i('User name'), true],
        'email' => [_i('E-mail address'), true],
        'ldap' => [_i('LDAP'), true],
    ]) }}
@endsection
