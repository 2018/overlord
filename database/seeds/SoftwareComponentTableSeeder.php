<?php

use Illuminate\Database\Seeder;
use App\Models\SoftwareComponent;

class SoftwareComponentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $class = SoftwareComponent::class;
        $data = [
            ['id' => 1, 'software_component_type_id' => 1, 'name' => 'logmanager'],
            ['id' => 2, 'software_component_type_id' => 2, 'name' => 'wes'],
            ['id' => 3, 'software_component_type_id' => 3, 'name' => 'vmware'],
            ['id' => 4, 'software_component_type_id' => 4, 'name' => 'sap'],
            ['id' => 5, 'software_component_type_id' => 5, 'name' => 'sql'],
            ['id' => 6, 'software_component_type_id' => 6, 'name' => 'checkpoint'],
            ['id' => 7, 'software_component_type_id' => 7, 'name' => 'dashboards'],
        ];
        foreach ($data as $aData) {
            $model = new $class();
            if (!is_null($model1 = $model::find($aData['id']))) {
                $model = $model1;
            }
            $model->fill($aData);
            if (!$model->validate()) {
                $this->command->getOutput()->writeln("<error>Errors: </error>");
                dd($model->getErrors());
            } else {
                $model->save();
            }
        }
    }
}
