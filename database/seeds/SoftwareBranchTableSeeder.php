<?php

use Illuminate\Database\Seeder;
use App\Models\SoftwareBranch;

class SoftwareBranchTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $class = SoftwareBranch::class;
        $data = [
            ['id' => 1, 'type' => 'Stable'],
            ['id' => 2, 'type' => 'Beta'],
            ['id' => 3, 'type' => 'Testing'],
        ];
        foreach ($data as $aData) {
            $model = new $class();
            if (!is_null($model1 = $model::find($aData['id']))) {
                $model = $model1;
            }
            $model->fill($aData);
            if (!$model->validate()) {
                $this->command->getOutput()->writeln("<error>Errors: </error>");
                dd($model->getErrors());
            } else {
                $model->save();
            }
        }
    }
}
