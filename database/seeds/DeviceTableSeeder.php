<?php

use App\Models\Device;
use App\Models\DeviceHistory;
use Illuminate\Database\Seeder;

class DeviceTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $class = Device::class;
        $inputData = json_decode(file_get_contents(storage_path('devices.json')));
        foreach ($inputData as $aData) {
            $statusId = array_get($aData, 'status_id') == 4 ? 3 : 1;
            $platform = \App\Models\Platform::where('name', data_get($aData, 'platform'))->first();
            $device = Device::where('serial', data_get($aData, 'serial'))->first();
            $param = [
                'customer_id' => 1,
                'software_branch_id' => 1,
                'device_status_id' => $statusId,
                'platform_id' => $platform->getAttribute('id'),
                'serial' => data_get($aData, 'serial'),
                'created_at' => data_get($aData, 'created_at'),
                'updated_at' => data_get($aData, 'updated_at'),
            ];
            $model = new $class();
            if (!is_null($model1 = $model::find(data_get($device, 'id')))) {
                $model = $model1;
            }
            $model->fill($param);
            $model->rewriteRule('serial', 'required');
            if (!$model->validate()) {
                $this->command->getOutput()->writeln("<error>Errors: </error>");
                dd($model->getErrors());
            } else {
                $model->save();
            }
            $this->createDeviceHistory($model, $aData);
        }
    }

    protected function createDeviceHistory($device, $aData)
    {
        $class = DeviceHistory::class;
        $deviceHistory = DeviceHistory::where('device_id', data_get($device, 'id'))->first();
        $param = [
            'device_id' => $device->getAttribute('id'),
            'ip' => data_get($aData, 'ip'),
            'ptr' => gethostbyaddr(data_get($aData, 'ip')),
            'created_at' => data_get($aData, 'last_check')
        ];
        $model = new $class();
        if (!is_null($model1 = $model::find(data_get($deviceHistory, 'id')))) {
            $model = $model1;
        }
        $model->fill($param);
        if (!$model->validate()) {
            $this->command->getOutput()->writeln("<error>Errors: </error>");
            dd($model->getErrors());
        } else {
            $model->save();
        }
        $this->createDeviceHistorySoftwareVersion($model);
    }

    protected static function createDeviceHistorySoftwareVersion($model)
    {
        $model->softwareVersions()->sync([1]);
    }
}
