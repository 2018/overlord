<?php

use Illuminate\Database\Seeder;
use App\Models\SoftwareVersion;

class SoftwareVersionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $class = SoftwareVersion::class;
        $data = [
            [
                'id' => 1,
                'software_branch_id' => 1,
                'software_component_id' => 1,
                'version' => '000000.000000.000000',
                'file_name' => 'default.img',
                'is_enabled' => true,
            ],
        ];
        foreach ($data as $aData) {
            $model = new $class();
            if (!is_null($model1 = $model::find($aData['id']))) {
                $model = $model1;
            }
            $model->fill($aData);
            if (!$model->validate()) {
                $this->command->getOutput()->writeln("<error>Errors: </error>");
                dd($model->getErrors());
            } else {
                $model->save();
                $model->platforms()->sync(\App\Models\Platform::where('id' ,'>' ,0)->pluck('id'));
            }
        }
    }
}
