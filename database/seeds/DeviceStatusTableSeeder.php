<?php

use Illuminate\Database\Seeder;
use App\Models\DeviceStatus;

class DeviceStatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $class = DeviceStatus::class;
        $data = [
            ['id' => 1, 'name' => 'Ok'],
            ['id' => 2, 'name' => 'Fail'],
            ['id' => 3, 'name' => 'Removed'],
        ];
        foreach ($data as $aData) {
            $model = new $class();
            if (!is_null($model1 = $model::find($aData['id']))) {
                $model = $model1;
            }
            $model->fill($aData);
            if (!$model->validate()) {
                $this->command->getOutput()->writeln("<error>Errors: </error>");
                dd($model->getErrors());
            } else {
                $model->save();
            }
        }
    }
}
