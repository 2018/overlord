<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserTableSeeder::class);
        $this->call(DeviceStatusTableSeeder::class);
        $this->call(SoftwareBranchTableSeeder::class);
        $this->call(PlatformTableSeeder::class);
        $this->call(SupportTypeTableSeeder::class);
        $this->call(SoftwareComponentTypeTableSeeder::class);
        $this->call(SoftwareComponentTableSeeder::class);
        $this->call(SoftwareVersionTableSeeder::class);
        $this->call(CountryTableSeeder::class);
        $this->call(PartnerTableSeeder::class);
        $this->call(CustomerTableSeeder::class);
    }
}
