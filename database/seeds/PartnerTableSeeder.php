<?php

use App\Models\Partner;
use Illuminate\Database\Seeder;

class PartnerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $class = Partner::class;
        $data = [
            ['id' => 1, 'country_id' => 1, 'name' => 'unknown', 'type' => 'unknown'],
        ];
        foreach ($data as $aData) {
            $model = new $class();
            if (!is_null($model1 = $model::find($aData['id']))) {
                $model = $model1;
            }
            $model->fill($aData);
            if (!$model->validate()) {
                $this->command->getOutput()->writeln("<error>Errors: </error>");
                dd($model->getErrors());
            } else {
                $model->save();
            }
        }
    }
}
