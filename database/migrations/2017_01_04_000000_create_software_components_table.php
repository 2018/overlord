<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSoftwareComponentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'software_components', function (Blueprint $table) {
                $table->increments('id');
                $table->unsignedInteger('software_component_type_id')->index();
                $table->foreign('software_component_type_id')->references('id')->on('software_component_types');
                $table->string('name', 45)->unique();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('software_components');
    }
}
