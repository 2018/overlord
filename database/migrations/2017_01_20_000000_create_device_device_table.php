<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeviceDeviceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'device_device', function (Blueprint $table) {
                $table->unsignedInteger('device_id')->index();
                $table->foreign('device_id')->references('id')->on('devices')->onDelete('cascade');
                $table->unsignedInteger('devices_id')->index();
                $table->foreign('devices_id')->references('id')->on('devices')->onDelete('cascade');
                $table->unique(['device_id', 'devices_id']);
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('device_device');
    }
}
