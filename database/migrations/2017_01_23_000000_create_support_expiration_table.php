<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSupportExpirationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'support_expiration', function (Blueprint $table) {
                $table->increments('id');
                $table->unsignedInteger('device_id')->index();
                $table->foreign('device_id')->references('id')->on('devices');
                $table->unsignedInteger('support_type_id')->index();
                $table->foreign('support_type_id')->references('id')->on('support_types');
                $table->string('description', 255)->nullable();
                $table->date('expiration_date');
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('support_expiration');
    }
}
