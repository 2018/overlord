<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlatformSoftwareVersionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'platform_software_version', function (Blueprint $table) {
                $table->unsignedInteger('platform_id')->index();
                $table->foreign('platform_id')->references('id')->on('platforms')->onDelete('cascade');
                $table->unsignedInteger('software_version_id')->index();
                $table->foreign('software_version_id')->references('id')->on('software_versions')->onDelete('cascade');
                $table->unique(['platform_id', 'software_version_id']);
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('platform_software_version');
    }
}
