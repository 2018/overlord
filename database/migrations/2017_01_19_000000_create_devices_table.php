<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDevicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'devices', function (Blueprint $table) {
                $table->increments('id');
                $table->unsignedInteger('customer_id')->index();
                $table->foreign('customer_id')->references('id')->on('customers');
                $table->unsignedInteger('software_branch_id')->index();
                $table->foreign('software_branch_id')->references('id')->on('software_branches');
                $table->unsignedInteger('device_status_id')->index();
                $table->foreign('device_status_id')->references('id')->on('device_statuses');
                $table->unsignedInteger('platform_id')->index();
                $table->foreign('platform_id')->references('id')->on('platforms');
                $table->uuid('uuid')->unique()->nullable();
                $table->string('serial', 45)->unique();
                $table->string('description', 255)->nullable();
                $table->string('hw_model', 255)->nullable();
                $table->timestamps();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('devices');
    }
}
