<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'contacts', function (Blueprint $table) {
                $table->increments('id');
                $table->unsignedInteger('customer_id')->index();
                $table->foreign('customer_id')->references('id')->on('customers');
                $table->unsignedInteger('partner_id')->index();
                $table->foreign('partner_id')->references('id')->on('partners');
                $table->string('name', 45)->unique();
                $table->string('email', 191)->unique();
                $table->string('phone', 60);
                $table->string('description', 255)->nullable();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contacts');
    }
}
