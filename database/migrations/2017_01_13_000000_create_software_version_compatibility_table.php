<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSoftwareVersionCompatibilityTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'software_version_compatibility', function (Blueprint $table) {
                $table->increments('id');
                $table->unsignedInteger('software_version_id')->index();
                $table->foreign('software_version_id')->references('id')->on('software_versions')->onUpdate('cascade')->onDelete('cascade');
                $table->unsignedInteger('min_version_id')->nullable();
                $table->foreign('min_version_id')->references('id')->on('software_versions')->onUpdate('cascade')->onDelete('cascade');
                $table->unsignedInteger('max_version_id')->nullable();
                $table->foreign('max_version_id')->references('id')->on('software_versions')->onUpdate('cascade')->onDelete('cascade');
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('software_version_compatibility');
    }
}
