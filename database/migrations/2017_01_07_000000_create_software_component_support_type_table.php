<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSoftwareComponentSupportTypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'software_component_support_type', function (Blueprint $table) {
                $table->unsignedInteger('software_component_id')->index();
                $table->foreign('software_component_id')->references('id')->on('software_components')->onDelete('cascade');
                $table->unsignedInteger('support_type_id')->index();
                $table->foreign('support_type_id')->references('id')->on('support_types')->onDelete('cascade');
                $table->unique(['software_component_id', 'support_type_id'], 'scst_software_component_id_support_type_id_unique');
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('software_component_support_type');
    }
}
