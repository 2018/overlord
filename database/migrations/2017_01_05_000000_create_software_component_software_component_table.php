<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSoftwareComponentSoftwareComponentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'software_component_software_component', function (Blueprint $table) {
                $table->unsignedInteger('software_component_id');
                $table->index('software_component_id', 'scsc_software_component_id_index');
                $table->foreign('software_component_id', 'scsc_software_component_id_foreign')->references('id')->on('software_components')->onDelete('cascade');
                $table->unsignedInteger('software_components_id');
                $table->index('software_component_id', 'scsc_software_components_id_index');
                $table->foreign('software_components_id', 'scsc_software_components_id_foreign')->references('id')->on('software_components')->onDelete('cascade');
                $table->unique(['software_component_id', 'software_components_id'], 'scsc_software_component_id_software_components_id_unique');
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('software_component_software_component');
    }
}
